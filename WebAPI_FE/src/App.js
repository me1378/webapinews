import './App.css';
import Header from "./components/Header";
import Table from "./components/Table";
import Dropdown from './components/Dropdown';

function App() {
  return (
    <div className="Header">
      <Header></Header>
      <hr></hr>
      <div style={{float: 'left', fontSize:'20px'}}>
        Ultimas Noticias
      </div>
      <div style={{float: 'right'}}>
        <Dropdown></Dropdown>
      </div>
      <br></br>
      <div className="container">
        <div className="row">
          <Table></Table>
        </div>
      </div>
    </div>
  );
}

export default App;
