import React, { Component } from "react";
import Newscard from "../Newscard";
import axios from "axios";

export default class Table extends Component {
    constructor(props) {
        super(props);
    }    

    state = {
        news: [],
        search: false
    }

    componentDidMount = () => {
        this.getNews("https://localhost:44352/headlines", "");
    }

    getNews = (endpoint, query) => {
        let config = {
            headers: {"Access-Control-Allow-Origin": "*"}
        }

        axios.get(endpoint+query, config)
            .then((response) => {
            const data = response.data;
            this.setState({news:data});
        })
    }

    handleKeyPress = (event) => {
        if(event.key === 'Enter'){
            this.getNews("https://localhost:44352/search", "?query="+event.target.value);
        }
    }

    render() {
        return (
            <div>
                <button onClick={ () => { this.setState(prevState => ({search: !prevState.search})) } }>
                    Buscador
                </button>
                { this.state.search &&
                    <input type="text" onKeyPress={this.handleKeyPress} /> 
                }
                <table>
                    <tbody>
                        <tr>
                            <td>
                                {Object.keys(this.state.news).map((key) => (
                                <div className="container">
                                    <Newscard values={this.state.news[key]} />
                                </div>
                                ))}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}