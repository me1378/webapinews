import React, { Component } from "react";
import Moment from 'moment';

export default class Newscard extends Component {
constructor(props) {
    super(props);
}

render() {
    return (
        <div className="flex-container-row">
            <img src={this.props.values.urlToImage} width="200" height="200" />
              <table className="table">
                <tbody>
                  <tr className="headline" style={{"float": "left", "fontSize":"20px"}}>
                    <td>{this.props.values.title}</td>
                  </tr>
                  <tr className="description" style={{"fontSize": "20px"}}>
                    <td>{this.props.values.description}</td>
                  </tr>
                  <tr className="date" style={{"fontSize":"15px"}}>
                    <td>{Moment(this.props.values.publishedAt).format('d MMM')}
                    <a href={this.props.values.url} style={{"fontSize": "15px", "float":"right"}}>Ver Más</a></td>
                  </tr>
                </tbody>
              </table>
        </div>
    );
  }
}