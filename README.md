# NewsApi Basic FE / BE Example

The project is a very basic FE <-> BE example of a C# API returning feed from newsapi (https://newsapi.org) and consumed by a React FE.



## FE Project

The FrontEnd project was created with [create-react-app](https://github.com/facebook/create-react-app). It was in our interest to go beyond basic css/html styling for the UI using other libraries (i.e. [styled-components](https://styled-components.com/)) but time constraints prevented us from doing so. 

The project uses [moment](https://momentjs.com/) for date time parsing.



## BE Project

The BackEnd project was created with ASP.NET Core using NET Core 6. We started from the ASP.NET Core Web API Visual Studio template that comes with the ASP.NET and web development workload for Visual Studio 2022.



## Requirements

- Visual Studio 2022 with the ASP.NET and web development workload
- NET Core 6
- NodeJS 16.13.1



## Running the project

1. Clone the repository
2. Install the requirements
3. Open the Solution `< REPO DIR >/WebAPI_BE/WebAPI_BE.sln` in Visual Studio 2022
4. Hit F5 to start debugging the project. Should restore any nuget packages, then open an IIS instance and a web browser with Swagger at https://localhost:44352/swagger/index.html
5. Open a terminal at `< REPO DIR >/WebAPI_FE`
6. Run `npm install`
7. Run `npm start` to start the FE site. Should open a web browser at http://localhost.3000



## Known Issues 

- There's no pagination in the site for the news
- The dropdown is not functional in the site
- The date parsing for the articles is returning the wrong date

