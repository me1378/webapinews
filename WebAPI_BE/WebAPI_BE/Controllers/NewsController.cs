﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net;
using WebAPI_BE.Helpers;
using WebAPI_BE.Models;

namespace WebAPI_BE.Controllers
{
    /// <summary>
    /// Controller for the News endpoints
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class NewsController : ControllerBase
    {
        private readonly string BASE_URL = "https://newsapi.org/v2/";

        private HttpClient HttpClient;

        public NewsController(IConfiguration configuration)
        {
            HttpClient = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate });
            HttpClient.DefaultRequestHeaders.Add("user-agent", "News-API-csharp/0.1");
            HttpClient.DefaultRequestHeaders.Add("x-api-key", configuration["NewsApiKey"]);
        }

        [EnableCors]
        [HttpGet("/headlines", Name = "TopArticles")]
        public IEnumerable<Article> Get()
        {
            return MakeRequest("top-headlines", "language=es").Result.ToArray();
        }

        [EnableCors]
        [HttpGet("/search", Name = "SearchArticles")]
        public IEnumerable<Article> Get([FromQuery] string query)
        {
            return MakeRequest("top-headlines", "q=" + query).Result.ToArray();
        }

        private async Task<List<Article>> MakeRequest(string endpoint, string querystring)
        {
            // make the http request
            var httpRequest = new HttpRequestMessage(HttpMethod.Get, BASE_URL + endpoint + "?" + querystring);
            var httpResponse = await HttpClient.SendAsync(httpRequest);

            var json = await httpResponse.Content.ReadAsStringAsync();

            // convert the json to an obj
            var requestResult = JsonConvert.DeserializeObject<RequestResult>(json);
            if (requestResult.Status.Equals(Status.Ok))
            {
                return requestResult.Articles;
            }
            else
            {
                var emptyArticle = new Article()
                {
                    ErrorMessage = requestResult.Code
                };
                return new List<Article>() { emptyArticle };
            }
        }
    }
}
