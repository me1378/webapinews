﻿namespace WebAPI_BE.Helpers
{
    /// <summary>
    /// Status of the request result
    /// </summary>
    public enum Status
    {
        Ok,
        Error
    }
}
