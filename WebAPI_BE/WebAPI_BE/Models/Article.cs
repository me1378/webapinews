﻿namespace WebAPI_BE.Models
{
    /// <summary>
    /// Represents a news article. Plus a varible to catch the error message
    /// for failed requests
    /// </summary>
    public class Article
    {
        public string? Author { get; set; }

        public string? Title { get; set; }   

        public string? Description { get; set; }

        public string? UrlToImage { get; set; }

        public string? Url { get; set; }

        public DateTime? PublishedAt { get; set;}

        public string? ErrorMessage { get; set; }
    }
}
