﻿using WebAPI_BE.Helpers;

namespace WebAPI_BE.Models
{
    /// <summary>
    /// Represents the result to the News API request
    /// </summary>
    public class RequestResult
    {
        public Status Status { get; set; }

        public string Code { get; set; }

        public int TotalResults { get; set; }

        public List<Article> Articles { get; set; }
    }
}
